package Test;

import org.junit.jupiter.api.Test;

import java.util.ArrayList;

public class NumberTwo {
    public void logic(int[] nums, int x) {
        ArrayList<Integer> list1 = new ArrayList<>();
        for (int i = 0; i < nums.length; i++) {
            for (int j = 0; j < nums.length; j++) {
                if((double) nums[i] / nums[j] == x){
                    list1.add(nums[i]);
                }
            }
        }

        ArrayList<Integer> list2 = new ArrayList<>();
        for(int nm : nums){
            if(!list1.contains(nm)){
                list2.add(nm);
            }
        }
        System.out.println(list2);
    }

    @Test
    void test(){
        logic(new int[] {1,2,3,4}, 4);
    }
}
