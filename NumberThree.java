package Test;

import org.junit.jupiter.api.Test;

import java.util.ArrayList;

public class NumberThree {

    public void findWord(String word, int x){
        ArrayList<String> list = new ArrayList<>();
        for(String wr : word.split(" ")) {
            if(wr.length() == x){
                list.add(wr);
            }
        }
        System.out.println(list);
    }

    @Test
    void test(){
        findWord("souvenir loud four lost", 4);
    }
}
