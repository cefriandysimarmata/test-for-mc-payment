package Test;

import org.junit.jupiter.api.Test;

import java.util.*;

public class NumberOne {
    public void logic(int[] nums) {
        int ans = 0, temp;
        for (int i = 0; i < nums.length; i++) {
            for (int j = 0; j < nums.length; j++) {
                if(nums[i] - nums[j] < 0){
                    temp = nums[j];
                    if (temp > ans) {
                        ans = temp;
                    }
                }
            }
        }
        ArrayList<Integer> list = new ArrayList<>();
        for(int x : nums){
            list.add(x);
        }
        list.retainAll(Collections.singleton(ans));

        System.out.println(list);
    }

    @Test
    void test(){
        logic(new int[] {3,1,2,4});
    }
}
